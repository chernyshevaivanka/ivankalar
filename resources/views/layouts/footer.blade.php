<?php
use Carbon\Carbon;
?>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

<footer class="text-muted">
    <div class="container-fluid">
        <div class="row">

            <div class="col-sm-12 col-md-12 col-lg-4 col-xl-4 text-center">
                <li><a class="footer-nav-link" href="#">Link</a></li>
                <li><a class="footer-nav-link" href="#">Link</a></li>
                <li><a class="footer-nav-link" href="#">Link</a></li>
            </div>

            <div class="col-sm-12 col-md-12 col-lg-4 col-xl-4 text-center">
               <h5 class="footerprivacy">Delivery & Payments</h5>
                <h5 class="footerprivacy">Security & Privacy</h5>
            </div>

            <div class="col-sm-12 col-md-12 col-lg-4 col-xl-4 text-center ">
                <p class="footer_copy_right">&#9400;<?php echo date('Y'); ?> All rights reserved. Ivanka Chernysheva.  </p>
            </div>
        </div>
    </div>

</footer>




