@extends('layout')

@section('header')
@endsection


@section('content')

    <div class="col-12 cccontainer">
        <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
                <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="https://static.zara.net/photos//mkt/spots/ss20-north-new-collection/subhome-woman-02-//desktop-ss20-north-new-collection-img.st.jpg?ts=1578051023153" class="d-block w-100" alt="...">
                    <div class="carousel-caption d-none d-md-block">
                        <h5>New Woman</h5>
                    </div>
                </div>
                <div class="carousel-item">
                    <img src="https://static.zara.net/photos//mkt/spots/ss20-north-new-collection/subhome-kids-02//desktop-new-collection-kids-img-01.st.jpg?ts=1578052231664" class="d-block w-100" alt="...">
                    <div class="carousel-caption d-none d-md-block">
                        <h5>New Kids</h5>
                    </div>
                </div>
                <div class="carousel-item">
                    <img src="https://static.zara.net/photos//mkt/spots/ss20-north-new-collection/subhome-man-02//desktop-ss20-north-new-collection-img-uuid1.st.jpg?ts=1578051884484" class="d-block w-100" alt="...">
                    <div class="carousel-caption d-none d-md-block">
                        <h5>New Men</h5>
                    </div>
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
@endsection


@section('footer')
@endsection

